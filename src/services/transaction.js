const log = require('../util/log')
const { api, wallet } = require('../../config/cfg')
const user = require('../controller/userController')
const tc = require('../controller/transactionController')
const { hashTransaction, cDate } = require('../util/utilities')

async function userPurchaseCredit(ownerId, sponserId, amount, walletType, lockedAmount) {
  log.info(`Processing purchase credit: R$ ${amount / 100}, OwnerId: ${ownerId}`)
  let transaction_label = "eVoucher Credito"

  let card_id = await user.getDefautCardId(ownerId)
  if (!card_id.status) return { status: card_id.status, msg: card_id.msg }

  let userDetail = await user.getUserDetail(ownerId)
  if (!userDetail.status) return { status: userDetail.status, msg: userDetail.msg }

  log.info(`Information User: Card: ${card_id.result}, User: ${userDetail.result.name}`)
  let options = {
    uri: api.uri + api.module_url.orders,
    headers: {
      'Authorization': 'Basic ' + api.buffer64Secret,
      'Content-Type': 'application/json'
    },
    json: {
      "items": [
        { amount, description: transaction_label, quantity: 1 },
      ],
      "customer": {
        "name": userDetail.result.name,
        "email": userDetail.result.email_address
      },
      "payments": [
        {
          "payment_method": "credit_card",
          "credit_card": {
            "recurrence": false,
            "installments": 1,
            "statement_descriptor": transaction_label,
            "card_id": card_id.result
          }
        }
      ]
    }
  }
  let data = await tc.pay(options, ownerId, sponserId, transaction_label, walletType, lockedAmount)
  return data
}

async function transferAmountP2P(source_id, recipient_id, amount, walletType, lockedAmount) {

  log.info(`Checking exist user source_id: ${source_id}`)
  let userDetailSource = await user.getUserDetail(source_id)
  if (!userDetailSource.status) return { status: userDetailSource.status, msg: userDetailSource.msg }

  log.info(`Checking exist user recipient_id: ${recipient_id}`)
  let userDetailRecipient = await user.getUserDetail(recipient_id)
  if (!userDetailRecipient.status) return { status: userDetailRecipient.status, msg: userDetailRecipient.msg }

  log.info(`Checking funds from source_id: ${source_id}`)
  let checkBalance = await tc.checkExistBalance(source_id, amount)
  if (!checkBalance.status) return { status: checkBalance.status, msg: checkBalance.msg }

  var transaction_label = `Transferência realizada`
  var hash_transaction = hashTransaction(
    source_id
    + recipient_id
    + cDate.date_time
    + transaction_label
    + amount
  )

  let source_data = {
    owner_id: source_id,
    sponsor_id: "",
    sponsored_id: recipient_id,
    charge_id: "",
    hash_transaction: hash_transaction,
    transaction_type_id: wallet.transaction_type.debit,
    amount: amount,
    transaction_date: cDate.date_time,
    transaction_label: transaction_label,
    wallet_type_id: walletType,
    locked_amount: lockedAmount

  }

  var transaction_label = `Transferência recebida`

  let recipient_data = {
    owner_id: recipient_id,
    sponsor_id: source_id,
    sponsored_id: "",
    charge_id: "",
    hash_transaction: hash_transaction,
    transaction_type_id: wallet.transaction_type.credit,
    amount: amount,
    transaction_date: cDate.date_time,
    transaction_label: transaction_label,
    wallet_type_id: walletType,
    locked_amount: lockedAmount
  }

  let data = await tc.transfer(source_data, recipient_data)
  return data
}

async function fetchCurrentBalance(user_id) {
  log.info(`Capturing balance from user_id: ${user_id}`)
  let userDetail = await user.getUserDetail(user_id)
  if (!userDetail.status) return { status: userDetail.status, msg: userDetail.msg }

  let data = await tc.fetchBalanceUser(user_id)
  if (!data.status) return { status: data.status, msg: data.msg }
  return data
}

module.exports = { userPurchaseCredit, transferAmountP2P, fetchCurrentBalance }