const fs = require('fs')
const { log } = require('../../config/cfg')
const { cDate } = require('./utilities')

function writeRegisterLog(msg) {
  let pathFile = log.path_log.replace("-DATE", `-${cDate.date}`)
  fs.appendFile(pathFile, `\n${msg}`, err => {
    if (err) {
      console.log(`> Failed write log`)
      console.log(err)
    }
  })
}

function info(msg) {
  if (log.levelEnable.info) {
    let msgComplete = `[INFO] - ${cDate.date_time} > ${msg}`
    writeRegisterLog(msgComplete)
  }
}

function warn(msg) {
  if (log.levelEnable.warn) {
    let msgComplete = `[WARN] - ${cDate.date_time} > ${msg}`
    writeRegisterLog(msgComplete)
  }
}

function critical(msg) {
  if (log.levelEnable.critical) {
    let msgComplete = `[CRITICAL] - ${cDate.date_time} > ${msg}`
    writeRegisterLog(msgComplete)
  }
}

function err(msg) {
  if (log.levelEnable.err) {
    let msgComplete = `[ERROR] - ${cDate.date_time} > ${msg}`
    writeRegisterLog(msgComplete)
  }
}

module.exports = {
  info, warn, critical, err
}