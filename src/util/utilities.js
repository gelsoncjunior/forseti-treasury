const dt = require('node-datetime')
const cfg = require('../../config/cfg')

function currentDate() {
  let dte = dt.create()
  let date_time = dte.format(cfg.internal.fmtDatetime)
  let date = dte.format(cfg.internal.fmtDate)
  return { date_time, date }
}

function convertDateAll(current_date, patternDate) {
  let dte = dt.create(current_date)
  let formattedDate = dte.format(patternDate)
  return formattedDate
}

function msgStatusReturn(status, msg, result) {
  return { status, msg, result }
}

function hashTransaction(value) {
  return Buffer.from(value).toString('base64')
}

module.exports = {
  cDate: {
    date_time: currentDate().date_time,
    date: currentDate().date
  },
  convertDateAll,
  msgStatusReturn,
  hashTransaction
}