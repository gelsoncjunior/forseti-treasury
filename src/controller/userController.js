const req = require('request')
const log = require('../util/log')
const { ords } = require('../../config/cfg')
const { msgStatusReturn } = require('../util/utilities')

async function getDefautCardId(ownerId) {
  return new Promise((resolve, reject) => {
    req.get({
      uri: ords.uri + ords.module_url.getDefaultCardId + ownerId,
      headers: {
        'Content-Type': 'application/json'
      },
    }, (err, res) => {
      if (!err && res.statusCode === 200) {
        if (JSON.parse(res.body).items.length != 0) {
          let data = JSON.parse(res.body).items[0].card_id
          resolve(msgStatusReturn(true, "", data))
        } else {
          log.critical(`No card_id returned to ownerId: ${ownerId}`)
          resolve(msgStatusReturn(false, `No card_id returned to ownerId: ${ownerId}`, ""))
        }
      } else if (!err && res.statusCode === 404) {
        log.critical(`No card_id returned to ownerId: ${ownerId}`)
        resolve(msgStatusReturn(false, `No card_id returned to ownerId: ${ownerId}`, ""))
      } else {
        log.err(err)
        resolve(msgStatusReturn(false, "Processing internal failed.", ""))
      }
    })
  })
}

async function getUserDetail(ownerId) {
  return new Promise((resolve, reject) => {
    req.get({
      uri: ords.uri + ords.module_url.getUserDetail + ownerId,
      headers: {
        'Content-Type': 'application/json'
      }
    }, (err, res) => {
      if (!err && res.statusCode === 200) {
        if (JSON.parse(res.body) != "") {
          var data = JSON.parse(res.body)
          var data = { name: data.name, email_address: data.email_address }
          resolve(msgStatusReturn(true, "", data))
        } else {
          log.critical(`No user_detail returned to ownerId: ${ownerId}`)
          resolve(msgStatusReturn(false, `No user_detail returned to ownerId: ${ownerId}`, ""))
        }
      } else if (!err && res.statusCode === 404) {
        log.critical(`No user_detail returned to ownerId: ${ownerId}`)
        resolve(msgStatusReturn(false, `No user_detail returned to ownerId: ${ownerId}`, ""))

      } else {
        log.critical(err)
        resolve(msgStatusReturn(false, "Processing internal failed.", ""))
      }
    })
  })
}

module.exports = { getDefautCardId, getUserDetail }