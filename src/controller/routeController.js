const tr = require('../services/transaction')
const app = require('../../config/server')
const log = require('../util/log')

function buyCredit() {
  let rt = "/wallet/pay/credit/"
  console.log(`Route (${rt}) Running`)
  app.post(rt, (req, res) => {
    let amount = req.body.amount
    let ownerId = req.body.owner_id
    let sponserId = req.body.sponser_id ? null : ownerId
    let walletType = req.body.wallet_type
    let lockedAmount = req.body.locked_amount
    tr.userPurchaseCredit(ownerId, sponserId, amount, walletType, lockedAmount).then((data) => {
      res.send(data)
    })
  })
}

function transferCredit() {
  let rt = "/wallet/transfer/"
  console.log(`Route (${rt}) Running`)
  app.post(rt, (req, res) => {
    let amount = req.body.amount
    let sourceId = req.body.source_id
    let recipientId = req.body.recipient_id
    let walletType = req.body.wallet_type
    let lockedAmount = req.body.locked_amount
    tr.transferAmountP2P(sourceId, recipientId, amount, walletType, lockedAmount).then((data) => {
      res.send(data)
    })
  })
}

function userBalance() {
  let rt = "/wallet/balance/:user_id"
  console.log(`Route (${rt}) Running`)
  app.get(rt, (req, res) => {
    let userId = req.params.user_id
    tr.fetchCurrentBalance(userId).then((data) => {
      res.send(data)
    })
  })
}

module.exports = { buyCredit, transferCredit, userBalance }