const req = require('request')
const log = require('../util/log')
const { wallet, internal, ords } = require('../../config/cfg')
const { msgStatusReturn, convertDateAll, hashTransaction } = require('../util/utilities')

async function pay(options, ownerId, sponsorId, transaction_label, wallet_type, locked_amount) {

  return new Promise((resolve, reject) => {
    req.post(options, (err, res) => {
      if (!err && res.statusCode === 200) {
        var data = res.body.charges[0]
        let transaction_date = convertDateAll(data.created_at, internal.fmtDatetime)
        var data = {
          owner_id: ownerId,
          sponsor_id: sponsorId,
          sponsored_id: "",
          charge_id: data.id,
          hash_transaction: hashTransaction(ownerId + sponsorId + transaction_date + transaction_label + data.amount),
          transaction_type_id: wallet.transaction_type.credit,
          amount: data.amount,
          status: data.status,
          transaction_date,
          transaction_label,
          wallet_type_id: wallet_type,
          locked_amount
          //select to_char(to_timestamp('2020-02-20T22:43:07Z','yyyy-mm-dd"T"hh24:mi:ss"Z"'),'DD-MM-RR HH24:MI:SS') from dual
        }
        if (data.status == "paid") {
          log.info(`Payment ${data.status} - hashId: ${data.hash_transaction}`)
          regInvoice(data)
          resolve(msgStatusReturn(true, `Payment ${data.status} - hashId: ${data.hash_transaction}`, data))
        } else {
          log.critical("Payment failed")
          resolve(msgStatusReturn(false, `Payment ${data.status} - hashId: ${data.hash_transaction}`, data))
        }
      } else {
        let rErr = null ? "" : ""
        log.err(`Processing internal failed ${rErr}`)
        resolve(msgStatusReturn(false, `Processing internal failed ${rErr}`, ""))
      }
    });
  })
}

async function transfer(source_data, recipient_data) {
  var data = await regInvoice(source_data)
  if (!data.status) return { status: data.status, msg: data.msg }

  var data = await regInvoice(recipient_data)
  if (!data.status) return { status: data.status, msg: data.msg }

  return { status: data.status, msg: data.msg }

}

async function regInvoice(data) {

  return new Promise((resolve, reject) => {
    let dataInvoice = {
      owner_id: parseInt(data.owner_id || 0),
      sponsor_id: parseInt(data.sponsor_id || 0),
      sponsored_id: parseInt(data.sponsored_id || 0),
      charge_id: data.charge_id,
      hash_transaction: data.hash_transaction,
      transaction_type_id: data.transaction_type_id,
      amount: data.amount,
      transaction_date: data.transaction_date,
      transaction_description: data.transaction_label,
      wallet_type_id: parseInt(data.wallet_type_id || 0),
      locked_amount: parseInt(data.locked_amount || 0)
    }
    console.log(dataInvoice)
    req.post({
      headers: { "content-type": "application/x-www-form-urlencoded" },
      url: ords.uri_hml + ords.module_url.registerCredit,
      form: dataInvoice
    }, (err, res) => {
      console.log(res.body)
      if (!err && res.statusCode === 200) {
        log.info(`Invoice registred`)
        resolve(msgStatusReturn(true, `Successful register Invoice`, ""))
      } else {
        log.critical(`Invoice failed when registred`)
        resolve(msgStatusReturn(false, `Failed register Invoice`, ""))
      }
    })
  })

}

async function checkExistBalance(user_id, amount) {
  return new Promise((resolve, reject) => {
    req.get(ords.uri_hml + ords.module_url.getBalance + user_id, (err, res) => {
      if (!err && res.statusCode === 200) {
        let balance = JSON.parse(res.body).items[0].balance
        log.info(`Balance user_id captured : ${user_id} - [ ${balance} ] `)
        if (balance < amount) {
          log.warn(`User_id ${user_id} insufficient funds`)
          resolve(msgStatusReturn(false, `insufficient funds`, balance))
        } else {
          log.info(`User_id ${user_id} Balance captured with success`)
          resolve(msgStatusReturn(true, `Balance captured with success`, balance))
        }
      } else if (!err && res.statusCode === 404) {
        log.critical(`Failed capture balance user_id : ${user_id}`)
        resolve(msgStatusReturn(false, `Failed capture balance user_id : ${user_id}`, ""))
      } else {
        log.critical(`Failed capture balance user_id : ${user_id}`)
        resolve(msgStatusReturn(false, `Failed capture balance user_id : ${user_id}`, ""))
      }
    })
  })
}

async function fetchBalanceUser(user_id) {
  return new Promise((resolve, reject) => {
    req.get(ords.uri_hml + ords.module_url.getBalance + user_id, (err, res) => {
      if (!err && res.statusCode === 200) {
        let balance = JSON.parse(res.body).items[0].balance
        log.info(`User_id: ${user_id}, Balance: ${balance}`)
        resolve(msgStatusReturn(true, `Balance captured with success`, { "balance": balance }))
      } else {
        log.critical(`Failed captura balance user_id: ${user_id}`)
        resolve(msgStatusReturn(false, `Failed capture balance user_id : ${user_id}`, ""))
      }
    })
  })
}

module.exports = { pay, transfer, checkExistBalance, fetchBalanceUser }