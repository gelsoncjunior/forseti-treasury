const route = require('./src/controller/routeController')

function wallet() {
  route.buyCredit()
  route.transferCredit()
  route.userBalance()
}

function main() {
  wallet()
}

main()