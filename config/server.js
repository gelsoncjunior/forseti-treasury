const express = require('express')
const bodyParser = require("body-parser")
const { server } = require('./cfg')
const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.listen(server.port_listener, () => {
  console.log(`> Running On: ${server.port_listener}`)
})

module.exports = app