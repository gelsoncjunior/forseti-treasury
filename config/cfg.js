const internal = {
  fmtDatetime: "d-m-Y H:M:S",
  fmtDate: "d-m-Y"
}

const server = {
  port_listener: 3000
}

const wallet = {
  transaction_type: {
    credit: 1,
    debit: 2,
  },
}

const api = {
  key_secret: "sk_r7DzgbXhW6tjnwXJ",
  key_public: "pk_QxygEneiOSMLPvne",
  buffer64Secret: Buffer.from("sk_r7DzgbXhW6tjnwXJ" + ":").toString('base64'),
  uri: "https://api.mundipagg.com/core/v1",
  module_url: {
    orders: "/orders",
    getCustomer: "/customers"
  }
}

const api_hml = {
  key_secret: "sk_test_gaDJWXet8lFbLMmR",
  key_public: "pk_test_K4OM1eik2sr0MJ7Q",
  buffer64Secret: Buffer.from("sk_test_gaDJWXet8lFbLMmR" + ":").toString('base64'),
  uri: "https://api.mundipagg.com/core/v1",
  module_url: {
    orders: "/orders",
    getCustomer: "/customers"
  }
}

const ords = {
  uri: "http://apps.cubo.mobi:8001/ords/pcms/PCMS",
  uri_hml: "http://192.168.88.122:8080/ords/pcms",
  module_url: {
    getDefaultCardId: "/user/card/get_default/",
    getUserDetail: "/users/",
    registerCredit: "/v1/register_credit/",
    getBalance: "/v1/balance/"
  }
}

const log = {
  path_log: "logs/forset-treasury-DATE.out",
  levelEnable: {
    info: true,
    warn: true,
    critical: true,
    err: true
  }
}

module.exports = { wallet, api, api_hml, hathor, ords, server, log, internal }
